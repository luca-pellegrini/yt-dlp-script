#!/usr/bin/python3
"""yt-dlp-script: un semplice script per scaricare in pochi passaggi video
e musica da YouTube con yt-dlp."""

import os
import os.path
import readline
import shutil
import sys
import subprocess
import time


_now = time.strftime("%Y.%m.%d-%H.%M.%S")

"""Profili predefiniti
Dizionario contenente coppie del tipo: N:("Display name", ["argomenti", "per", "yt-dlp"])"""
music_profiles = {
    "m1": ("Musica 1 (Opus, sorted by artist/album)",
          ["-f", "251/ba[ext=webm]", "--restrict-filenames", "--embed-metadata", "--no-embed-chapters", "--remux-video", "ogg", "-o", "/Data/Musica/Downloads/%(artist)s/%(album)s/%(playlist_index)s %(title)s.%(ext)s"]),
    "m2": ("Musica 2 (Opus, not sorted)",
          ["-f", "251/ba[ext=webm]", "--restrict-filenames", "--embed-metadata", "--no-embed-chapters", "--remux-video", "ogg", "-o", "/Data/Musica/Downloads/Artisti_Vari/%(artist)s - %(title)s.%(ext)s"]),
    "m3": ("Musica 3 (Opus, Classica)",
          ["-f", "251/ba[ext=webm]", "--restrict-filenames", "--embed-metadata", "--no-embed-chapters", "--remux-video", "ogg", "-o", "/Data/Musica/Downloads/Classica/%(album)s/%(playlist_index)s %(title)s.%(ext)s"]),
    "m4": ("Musica 4 (AAC, sorted by artist/album)",
          ["-f", "140/ba[ext=m4a]", "--restrict-filenames", "--embed-metadata", "--no-embed-chapters", "-o", "/Data/Musica/Downloads/%(artist)s/%(album)s/%(playlist_index)s %(title)s.%(ext)s"]),
    "m5": ("Musica 5 (AAC, not sorted)",
          ["-f", "140/ba[ext=m4a]", "--restrict-filenames", "--embed-metadata", "--no-embed-chapters", "-o", "/Data/Musica/Downloads/Artisti_Vari/%(artist)s - %(title)s.%(ext)s"]),
    "m6": ("Musica 6 (AAC, Classica, sorted by album)",
          ["-f", "140/ba[ext=m4a]", "--restrict-filenames", "--embed-metadata", "--no-embed-chapters", "--restrict-filenames", "-o", "/Data/Musica/Downloads/Classica/%(album)s/%(playlist_index)s %(title)s.%(ext)s"]),
    "m7": ("Musica 7 (AAC, Classica, not sorted)",
          ["-f", "140/ba[ext=m4a]", "--restrict-filenames", "--embed-metadata", "--no-embed-chapters", "--restrict-filenames", "-o", "/Data/Musica/Downloads/Classica/%(album)s/%(playlist_index)s %(title)s.%(ext)s"]),
    }
video_profiles = {
    "v1": ("Video 1 (mp4 a 1080p + audio AAC migliore)",
          ["--restrict-filenames", "-f", "bv*[vcodec^=avc][height<=1080][protocol^=m3u8]+140/(bv*[vcodec^=avc][height<=1080]+ba[ext=m4a])[protocol^=http]", "--embed-metadata", "--no-embed-chapters", "--embed-thumbnail", "--write-description", "--remux-video", "mp4"]),
    "v2": ("Video 2 (mp4 a 720p + audio AAC migliore)",
          ["--restrict-filenames", "-f", "bv*[vcodec^=avc][height<=720][protocol^=m3u8]+140/(bv*[vcodec^=avc][height<=720]+ba[ext=m4a])[protocol^=http]", "--embed-metadata", "--no-embed-chapters", "--embed-thumbnail", "--write-description", "--remux-video", "mp4"]),
    "v3": ("Video 3 (RaiPlay, mp4)",
           ["--restrict-filenames", "-f", "b[height<=1080]/bv[height<=1080]+ba", "--embed-metadata", "--no-embed-chapters"]),
    "v4": ("Video 4 (solo audio, AAC migliore)",
           ["--restrict-filenames", "-f", "140/ba[ext=m4a][protocol^=http]", "--embed-metadata", "--no-embed-chapters", "--write-description"])
    }
music_profiles_keys = music_profiles.keys()
video_profiles_keys = video_profiles.keys()


def check_for_ytdlp() -> bool:
    """Check whether yt-dlp is installed and available in PATH."""
    return not (shutil.which("yt-dlp") is None)
    # if shutil.which("yt-dlp") is None :
    #     return False
    # else:
    #     return True


def display_profiles_menu():
    """Display a menu listing available profiles."""
    menu_title = "Profili predefiniti:"
    print(menu_title)
    for i in music_profiles_keys:
        print(f" {i}) {music_profiles.get(i)[0]}")
    for j in video_profiles_keys:
        print(f" {j}) {video_profiles.get(j)[0]}")
    print("\n q) Esci dal programma\n")


def get_user_choice() -> tuple:
    prompt = "Scegliere un'opzione: "    
    while True:
        user_choice = input(prompt)
        if user_choice in exit_keys:
            sys.exit(0)
        elif user_choice in music_profiles_keys:
            ytdlp_args = ["yt-dlp"]
            ytdlp_args += ["--sleep-requests", "1.25", "--min-sleep-interval", "60", "--max-sleep-interval", "90"]
            ytdlp_args += music_profiles.get(user_choice)[1]
            break
        elif user_choice in video_profiles_keys:
            video_dir = os.path.expanduser(input("Cartella in cui salvare i video\n> "))
            if not os.path.isdir(video_dir):
                print("Errore: cartella non esistente", file=sys.stderr)
                continue
            ytdlp_args = ["yt-dlp"]
            ytdlp_args += ["--sleep-requests", "1.25", "--min-sleep-interval", "60", "--max-sleep-interval", "90"]
            ytdlp_args += video_profiles.get(user_choice)[1]
            ytdlp_args += ["--download-archive", f"{video_dir}/.archive", "-o", f"{video_dir}/%(upload_date>%Y.%m.%d)s %(title)s.%(ext)s"]
            subtitles_choice = input("Vuoi scaricare i sottotitoli (Inglese e Italiano, se disponibili) ? [s/N]\n> ")
            if subtitles_choice.lower() in yes_keys :
                ytdlp_args += ["--sub-langs", "en,it", "--write-subs"]
            break
    return tuple(ytdlp_args)


def download(ytdlp_args: tuple, log_file=None):
    """Chiede ripetutamente all'utente: un URL da scaricare, oppure
    il path di un file contenente un elenco di URL (uno per riga).
    Per ogni URL così ottenuto, invoca run_ytdlp(ytdlp_args).
    """
    print(f"Parametri attivi:\n{' '.join(ytdlp_args)}\n")
    while True:
        prompt = "Specificare URL (o file contenente elenco di URL validi, uno per riga):\n"
        user_reply = input(prompt)
        if user_reply in exit_keys:
            return
        elif user_reply.startswith("https://") or user_reply.startswith("http://"):
            run_ytdlp(ytdlp_args + (user_reply,), log_file)
        else:
            with open(user_reply, "r", encoding="UTF-8") as file:
                for line in file:
                    if line.startswith("https://") or line.startswith("http://"):
                        run_ytdlp(ytdlp_args + (line.rstrip("\n"),), log_file)


def run_ytdlp(ytdlp_args, log_file=None):
    """Run 'yt-dlp' with a set of command line arguments.
    
    Run yt-dlp, with the specified list of command line arguments, in a subprocess.
    If `log_file` is set, redirect stdout and stderr of subprocess to `log_file`,
    otherwise redirect them to `DEVNULL`.
    
    Keyword arguments:
    ytdlp_args -- list or tuple of command line arguments to be passed to yt-dlp
    log_file   -- if specified, a path to a log file (default None)
    """
    if log_file:
        out_file = open(log_file, "a", encoding="UTF-8")
    else:
        out_file = subprocess.DEVNULL
    #print(f"DEBUG: ytdlp_args = {ytdlp_args}")
    #print(f"DEBUG: out_file   = {out_file}")
    print(f"Downloading '{ytdlp_args[-1]}' ...")
    subprocess.run(ytdlp_args, stdout=out_file, stderr=subprocess.STDOUT, encoding="UTF-8")
    out_file.close()


if __name__ == "__main__":
    if not check_for_ytdlp():
        print("yt-dlp-script: Error: no 'yt-dlp' executable found in PATH", file=sys.stderr)
        sys.exit(2)
    
    """Set log's directory and logfile's pathname"""
    log_dir = os.path.join(os.path.expanduser("~"), ".cache", "yt-dlp-script")
    log_file = os.path.join(log_dir, f"{_now}.log")
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)
    exit_keys = ("q", "Q", "quit", "exit", "esci", "stop")
    yes_keys = ("y", "s", "yes", "sì", "si")
    
    """Display the menu and ask the user what to do"""
    display_profiles_menu()
    ytdlp_args = get_user_choice()
    #print(f"DEBUG: ytdlp_args = {ytdlp_args}")
    download(ytdlp_args, log_file)
    sys.exit(0)
